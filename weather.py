import pandas as pd
import pyarrow.parquet as pq
from pydrill.client import PyDrill
import missingno as msno


'''
# Loading the data into Pandas

Pandas is useful for me to review the data initially, i.e.

- Eyeball the nature of the data
- Review the heading
- Review data types
- Do the joining of the files
- Describe the statistics about the set
'''

weather_20160201 = pd.read_csv("weather.20160201.csv")
weather_20160301 = pd.read_csv("weather.20160301.csv")

# Quick review of data

weather_20160201.head()

weather_20160301.head()

'''
# Checking if both have the same columns

Both have the same column names.

'''

print(list(set(weather_20160201.columns).difference(set(weather_20160301.columns))))

# Checking the file types between both files

display(weather_20160201.info())
display(weather_20160301.info())

# Union the data sets

weather_complete = pd.concat([weather_20160201,weather_20160301])

# Review overall statistics

weather_complete.describe()

#ScreenTemperature has a min value of -99. I don't believe this to be a real data point rather a way of categorising a null value.

# Visualizing the nulls

msno.matrix(weather_complete)

# It looks like the critical fields to answer the questions are 100% available

# Convert to parquet

weather_complete.to_parquet('weather_parquet.gzip',compression='gzip')
'''
There is capability to add row group to to_parquet when using PyArrow, however

"**Configurations**

_Row group size:_

Larger row groups allow for larger column chunks which makes it possible to do larger sequential IO. Larger groups also require more buffering in the write path (or a two pass write). **We recommend large row groups (512MB - 1GB)**. Since an entire row group might need to be read, we want it to completely fit on one HDFS block. Therefore, HDFS block sizes should also be set to be larger. An optimized read setup would be: 1GB row groups, 1GB HDFS block size, 1 HDFS block per HDFS file.
..."

https://parquet.apache.org/documentation/latest/

The size of the file generated is under 512MB so I believe can exist within a _singular row group_.
'''

# Using Apache Drill to query Parquet

'''
_"Drill runs fastest against Parquet files because Parquet data representation is almost identical to how Drill represents data."_

https://drill.apache.org/docs/choosing-a-storage-format/

Drill is able to performantly query parquet files so I'll make use of this platform
'''

# Setting up Apache Drill in Docker
'''
If you're already familiar with Drill then you might want to skip this part :)

You can setup a test Apache Drill in Docker.

**docker run -it -v "$PWD":/tmp --name drill-1.17.0 -p 8047:8047 -t apache/drill:1.17.0 /bin/bash**

Note - Drill won't be able to access the files you wish to pass without mouting the directory.

Update $PWD (present working directory) to wherever you've saved the parquet file.

Map to the /tmp in the docker container and the file can be accessed in Drill with no additional config as tmp is a location already mapped.


To access the file in the query use the format (you can treat this like a reference to a table in a relational database)
'''

#dfs.`tmp`.`weather_parquet.gzip`

'''
- dfs refers to Drill's storage definition for 'local' storage files.


- `tmp` is a mapped location Drill is already configured for


- `weather_parquet.gzip` is the file created

Note the backticks
'''

# Answering the Questions

# Q1. - Which date was the hottest day?

#2016-03-17

'''
apache drill> WITH weather_mon_day_max AS
2..semicolon> (select year(ObservationDate) as year,
3..........)>  month(ObservationDate) as month,
4..........)>  day(ObservationDate) as day,
5..........)>  max(ScreenTemperature) as temp
6..........)>  from dfs.`tmp`.`weather_parquet.gzip`
7..........)>  group by year(ObservationDate),
8..........)>  month(ObservationDate),
9..........)>  day(ObservationDate)
10.........)>  order by month(ObservationDate), day(ObservationDate)
11.........)> )
12.semicolon> select * from weather_mon_day_max order by temp desc limit 1
13.semicolon> ;
14..........>
+------+-------+-----+------+
| year | month | day | temp |
+------+-------+-----+------+
| 2016 | 3     | 17  | 15.8 |
+------+-------+-----+------+
1 row selected (4.685 seconds)
'''

# Q2. What was the temperature that day?

# 15.8 (this ties up with the pandas describe above)

# Q3. In which region was the hottest day?
# Highland & Eilean Siar

'''
apache drill> select Region from dfs.`tmp`.`weather_parquet.gzip` where ScreenTemperature = 15.8;
+------------------------+
|         Region         |
+------------------------+
| Highland & Eilean Siar |
+------------------------+
1 row selected (1.161 seconds)
'''

# Using pydrill
'''
Drill provides an API accessible to Python. Using the API its possible to use the same queries
'''

drill = PyDrill(host='localhost', port=8047)

if not drill.is_active():
    raise ImproperlyConfigured('Please run Drill first')


print('# Q1/Q2. - Which date was the hottest day? / What was the temperature?')
q1 = drill.query('''
WITH weather_mon_day_max AS
(select
    year(ObservationDate) as year,
    month(ObservationDate) as month,
    day(ObservationDate) as day,
    max(ScreenTemperature) as temp
from dfs.`tmp`.`weather_parquet.gzip`
    group by year(ObservationDate),
    month(ObservationDate),
    day(ObservationDate)
    order by month(ObservationDate), day(ObservationDate)
)
select * from weather_mon_day_max order by temp desc limit 1
'''
                )

for result in q1:
    print(result)

print('-------------------------------------------------------------')

print('Q3. In which region was the hottest day?')

q3 = drill.query('''
select Region from dfs.`tmp`.`weather_parquet.gzip` where ScreenTemperature = 15.8
'''
                )

for result in q3:
    print(result)
